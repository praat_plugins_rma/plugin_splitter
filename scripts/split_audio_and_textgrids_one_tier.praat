# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc

@config.init: "../.log"

beginPause: "Split Sound and TextGrid where..."
  comment: "Input:"
  comment: "The directory where the files are stored..."
  sentence: "Audio directory", config.init.return$["extract_audio_and_textgrid.src_directory.sd"]
  sentence: "Textgrid directory", config.init.return$["extract_audio_and_textgrid.src_directory.tg"]
  comment: "The tier by which the files will be split"
  word: "Split tier", config.init.return$["extract_audio_and_textgrid.tier.split"]
  comment: "Output:"
  comment: "The directory where the resulting files will be stored..."
  sentence: "Save in", config.init.return$["extract_audio_and_textgrid.dst_directory"]
  comment: "Set the filename..."
  boolean: "Keep original filename", number(config.init.return$["extract_audio_and_textgrid.file_name.keep_original_filename"])
  word: "Prefix", config.init.return$["extract_audio_and_textgrid.file_name.prefix"]
  word: "Suffix", config.init.return$["extract_audio_and_textgrid.file_name.suffix"]
  comment: "Add a margin..."
  real: "Margin", number(config.init.return$["extract_audio_and_textgrid.file_name.margin"])
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "extract_audio_and_textgrid.src_directory.sd", audio_directory$
@config.setField: "extract_audio_and_textgrid.src_directory.tg", textgrid_directory$
@config.setField: "extract_audio_and_textgrid.tier.split", split_tier$
@config.setField: "extract_audio_and_textgrid.dst_directory", save_in$
@config.setField: "extract_audio_and_textgrid.file_name.prefix", prefix$
@config.setField: "extract_audio_and_textgrid.file_name.suffix", suffix$
@config.setField: "extract_audio_and_textgrid.file_name.margin", string$(margin)
@config.setField: "extract_audio_and_textgrid.file_name.keep_original_filename", string$(keep_original_filename)

fileList = Create Strings as file list: "directoryList", textgrid_directory$ + "/*.TextGrid"
nFiles = Get number of strings
for iFile to nFiles
  tg_name$ = object$[fileList, iFile]
  sd_name$ = tg_name$ - ".TextGrid" + ".wav"
  tg_dir$ = textgrid_directory$ + "/" + tg_name$
  sd_dir$ = audio_directory$ + "/" + sd_name$
  tg = Read from file: tg_dir$
  sd = Open long sound file: sd_dir$
  root_name$ = if keep_original_filename then tg_name$ - ".TextGrid" + "_" else "" fi
  selectObject: tg
  tb = Down to Table: "no", 16, "yes", "no"
  tb_extracted = nowarn Extract rows where column (text): "tier", "is equal to", split_tier$
  ## Go through all sentences
  for iRow to Object_'tb_extracted'.nrow
    tmin = object[tb_extracted, iRow, "tmin"]
    tmax = object[tb_extracted, iRow, "tmax"]
    split_label$ = object$[tb_extracted, iRow, "text"]
    if tmin and tmax
      ## Extract TextGrid
      selectObject: tg
      tg_extracted = Extract part: tmin, tmax, "no"
      Extend time: margin, "End"
      Extend time: margin, "Start"
      Shift times to: "start time", 0

      ## Extract audio
      selectObject: sd
      sd_extracted = Extract part: tmin-margin, tmax+margin, "no"

      file_id = 0
      repeat
        file_id += 1
        file_id$ = if file_id > 9 then "R'file_id'" else "R0'file_id'" fi
        fileDir$ = save_in$ + "/" + root_name$ + prefix$ + split_label$ + "_"+ suffix$ + file_id$
      until !fileReadable(fileDir$ + ".TextGrid")

      selectObject: sd_extracted
      Save as WAV file: fileDir$ + ".wav"
      selectObject: tg_extracted
      Save as text file: fileDir$ + ".TextGrid"

      removeObject: tg_extracted, sd_extracted
    endif
  endfor
  removeObject: tg, sd, tb, tb_extracted
endfor
removeObject: fileList

pauseScript: "The process has been completed successfully"