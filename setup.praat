# Copyright 2017 Rolando Muñoz Aramburú
if praatVersion < 6033
  appendInfoLine: "Plug-in name: Splitter"
  appendInfoLine: "Warning: This plug-in only works on Praat version above 6.0.32. Please, get a more recent version of Praat."
  appendInfoLine: "Praat website: http://www.fon.hum.uva.nl/praat/"
endif
Add menu command: "Objects", "Praat", "Splitter", "Goodies", 1, ""
Add menu command: "Objects", "Praat", "Split Sound and TextGrid...", "Splitter", 2, "scripts/split_audio_and_textgrids_one_tier.praat"
Add menu command: "Objects", "Praat", "Split Sound and TextGrid (conditional tier)...", "Splitter", 2, "scripts/split_audio_and_textgrids_conditional_tier.praat"
Add menu command: "Objects", "Praat", "-", "Splitter", 2, ""
Add menu command: "Objects", "Praat", "About", "Splitter", 2, "scripts/about.praat"