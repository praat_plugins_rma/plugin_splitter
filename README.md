# TextGrid Query

This is a Praat plug-in which provides a set of useful commands to split audio and annotation files.
 
After you have installed the plug-in, you can use it inmediately. To do it, go to `Praat > Goodies > Splitter` in the *fixed menu*. The followings commands should now be available:

